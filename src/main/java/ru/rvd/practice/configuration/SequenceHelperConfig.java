package ru.rvd.practice.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.rvd.practice.helper.SequenceHelper;
import ru.rvd.practice.settings.SequenceCondition;

@Configuration
public class SequenceHelperConfig {

    @Bean
    public SequenceHelper ascSequenceHelper() {
        return new SequenceHelper(SequenceCondition.ASCENDING);
    }

    @Bean
    public SequenceHelper descSequenceHelper() {
        return new SequenceHelper(SequenceCondition.DESCENDING);
    }
}
