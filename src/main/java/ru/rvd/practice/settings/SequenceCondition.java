package ru.rvd.practice.settings;

import java.util.function.BiPredicate;

public interface SequenceCondition {
    BiPredicate<Integer, Integer> ASCENDING = (currentNumber, lastNumber) -> currentNumber >= lastNumber;
    BiPredicate<Integer, Integer> DESCENDING = (currentNumber, lastNumber) -> currentNumber <= lastNumber;
}
