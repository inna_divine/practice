package ru.rvd.practice.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.rvd.practice.response.DoubleResponse;
import ru.rvd.practice.response.IntegerResponse;
import ru.rvd.practice.response.SequenceResponse;
import ru.rvd.practice.service.NumberService;

import javax.annotation.Resource;

@RestController
public class NumberController {

    @Resource
    private NumberService numberService;

    @GetMapping("/get_max_value")
    public IntegerResponse getMax(){
        return new IntegerResponse(numberService.getMax());
    }

    @GetMapping("/get_min_value")
    public IntegerResponse getMin(){
        return new IntegerResponse(numberService.getMin());
    }

    @GetMapping("/get_median")
    public IntegerResponse getMedian(){
        return new IntegerResponse(numberService.getMedian());
    }

    @GetMapping("/get_average")
    public DoubleResponse getAverage(){
        return new DoubleResponse(numberService.getAverage());
    }

    @GetMapping("/get_asc_seq")
    public SequenceResponse getAscendingSequences(){
        return new SequenceResponse(numberService.getAscendingSequences());
    }

    @GetMapping("/get_desc_seq")
    public SequenceResponse getDescendingSequences(){
        return new SequenceResponse(numberService.getDescendingSequences());
    }
}
