package ru.rvd.practice.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class BenchmarkAspect {

    @Around("@annotation(ru.rvd.practice.aspect.Benchmark)")
    public Object measureExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
        Object result = joinPoint.proceed();
        long endTime = System.currentTimeMillis();

        System.out.println("Метод {" + joinPoint.getSignature().getName() +
                "} выполнен за " + (endTime - startTime) + " мс.");
        return result;
    }
}
