package ru.rvd.practice.service;

import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import ru.rvd.practice.aspect.Benchmark;
import ru.rvd.practice.helper.SequenceHelper;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class NumberService {

    @Resource
    private ResourceLoader resourceLoader;

    @Resource
    private SequenceHelper ascSequenceHelper;

    @Resource
    private SequenceHelper descSequenceHelper;

    private List<Integer> numbers;

    @PostConstruct
    public void initDefaultSource() throws IOException {
        File defaultData = resourceLoader.getResource("classpath:data/10m.txt").getFile();
        try (Stream<String> lines = Files.lines(Paths.get(defaultData.toURI()))) {
            numbers = lines.map(Integer::valueOf).collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Benchmark
    public Integer getMin() {
        return numbers.parallelStream().min(Integer::compareTo).get();
    }

    @Benchmark
    public Integer getMax() {
        return numbers.parallelStream().max(Integer::compareTo).get();
    }

    @Benchmark
    public Integer getMedian() {
        return numbers.parallelStream()
                .mapToInt(Integer::intValue)
                .sorted()
                .skip(numbers.size() / 2)
                .limit(1)
                .findFirst().getAsInt();
    }

    @Benchmark
    public Double getAverage() {
        return numbers.parallelStream().mapToInt(Integer::intValue).average().getAsDouble();
    }

    @Benchmark
    public List<List<Integer>> getAscendingSequences() {
        return ascSequenceHelper.getSequences(numbers);
    }

    @Benchmark
    public List<List<Integer>> getDescendingSequences() {
        return descSequenceHelper.getSequences(numbers);
    }
}
