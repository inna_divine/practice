package ru.rvd.practice.response;

public class DoubleResponse {

    private Double value;

    public DoubleResponse(Double value) {
        this.value = value;
    }

    public Double getValue() {
        return value;
    }
}
