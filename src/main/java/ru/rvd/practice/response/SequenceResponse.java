package ru.rvd.practice.response;

import java.util.List;

public class SequenceResponse {

    private List<List<Integer>> sequences;

    public SequenceResponse(List<List<Integer>> sequences) {
        this.sequences = sequences;
    }

    public List<List<Integer>> getSequences() {
        return sequences;
    }
}
