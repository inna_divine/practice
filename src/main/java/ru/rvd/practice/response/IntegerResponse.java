package ru.rvd.practice.response;

public class IntegerResponse {

    private Integer value;

    public IntegerResponse(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }
}
