package ru.rvd.practice.helper;

import java.util.*;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

public class SequenceHelper {

    private BiPredicate<Integer, Integer> sequenceCondition;

    public SequenceHelper(BiPredicate<Integer, Integer> sequenceCondition) {
        this.sequenceCondition = sequenceCondition;
    }

    public List<List<Integer>> getSequences(List<Integer> numbers) {
        List<List<Integer>> allSequences = numbers
                .parallelStream()
                .collect(ArrayList::new, this::sequencesAccumulator, List::addAll);

        return getLongestSequences(allSequences);
    }

    private void sequencesAccumulator(List<List<Integer>> sequences, Integer currentNumber) {
        if (sequences.size() == 0) {
            sequences.add(new ArrayList<>(List.of(currentNumber)));
            return;
            //1 -> [ [1, 2], [-1, 0] ]
        }

        List<Integer> lastSequence = sequences.get(sequences.size() - 1);
        Integer lastNumber = lastSequence.get(lastSequence.size() - 1);
        if (sequenceCondition.test(currentNumber, lastNumber)) {
            lastSequence.add(currentNumber);
        } else {
            sequences.add(new ArrayList<>(List.of(currentNumber)));
        }
    }

    private List<List<Integer>> getLongestSequences(List<List<Integer>> sequences) {
        int maxSequenceSize = sequences
                .parallelStream()
                .mapToInt(List::size).max()
                .getAsInt();

        return sequences.parallelStream()
                .filter(sequence -> sequence.size() == maxSequenceSize)
                .collect(Collectors.toList());
    }
}
